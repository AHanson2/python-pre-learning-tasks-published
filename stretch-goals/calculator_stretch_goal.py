def calculator(a, b, operator):
    # ==============
    # Your code here
    num = 0
    if operator == "+":
        return format((a + b), 'b')      
    elif operator == "-":
        return format((a - b), 'b')
    elif operator == "*":
        return format((a * b), 'b')
    elif operator == "/":
        num = int(a / b)
        return format(num, 'b')
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
