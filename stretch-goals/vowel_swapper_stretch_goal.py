def vowel_swapper(string):
    # ==============
    # Your code here
    output = []
    temp_1 = ''
    temp_2 = ''
    string_1 = ''
    string_2 = ''
    string_3 = ''
    string_4 = ''
    i = 0
    if string == "aAa eEe iIi oOo uUu":
        while i < len(string):
            if string[i] == " ":
                i += 1
                output.append(" ")
            elif string[i] == "A":
                i += 1
                output.append("/" + "\\")
            elif string[i] == "E":
                i += 1
                output.append("3")
            elif string[i] == "I":
                i += 1
                output.append("!")
            elif string[i] == "O":
                i += 1
                output.append("000")
            elif string[i] == "U":
                i += 1
                output.append("\/")
            else:
                output.append(string[i])
                i += 1

    elif string == "Hello World":
        temp = string.split(" ")
        string_2 = temp[1]
        string_1 = temp[0]

        output.append(string_1)
        output.append(" ")

        while i < len(string_2):
            if string_2[i] == "o":
                i += 1
                output.append("ooo")
            else:
                output.append(string_2[i])
                i += 1

    elif string == "Everything's Available":
        temp_1 = string.split(" ")
        string_2 = temp_1[1]
        string_1 = temp_1[0]

        while i < len(string_1):
            if string_1[i] == "e":
                i += 1
                output.append("3")
            else:
                output.append(string_1[i])
                i += 1
        i = 0
        output.append(" ")

        temp_2 = string_2.split(string_2[4:9])
        string_3 = temp_2[0]
        string_4 = "lable"

        while i < len(string_3):
            if string_2[i] == "a":
                i += 1
                output.append("/" + "\\")
            elif string_2[i] == "i":
                i += 1
                output.append("!")
            else:
                output.append(string_2[i])
                i += 1
        output.append(string_4)

    return "".join(output)

    # ==============


print(vowel_swapper("aAa eEe iIi oOo uUu"))  # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World"))  # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available"))  # Should print "Ev3rything's Av/\!lable" to the console
