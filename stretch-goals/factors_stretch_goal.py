def factors(number):
    # ==============
    # Your code here
    str = "{} is a prime number"
    factors = []
    for i in range(1, number + 1):
        if number % i == 0 and i != number and i != 1:
            factors.append(i)
    if len(factors) == 0:
        return str.format(number)
    else:
        return factors
    # ==============


print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “13 is a prime number”
