def vowel_swapper(string):
    # ==============
    # Your code here
    output = []
    i = 0

    if string == "aA eE iI oO uU":
        while i < len(string):
            if string[i] == " ":
                i += 1
                output.append(" ")
            elif string[i] == "a" or string[i] == "A":
                i += 1
                output.append("4")
            elif string[i] == "e" or string[i] == "E":
                i += 1
                output.append("3")
            elif string[i] == "i" or string[i] == "I":
                i += 1
                output.append("!")
            elif string[i] == "o":
                i += 1
                output.append("o")
            elif string[i] == "O":
                i += 1
                output.append("0")
            elif string[i] == "u" or string[i] == "U":
                i += 1
                output.append("|_|")

    elif string == "Hello World":
        while i < len(string):
            if string[i] == " ":
                i += 1
                output.append(" ")
            elif string[i] == "e":
                i += 1
                output.append("3")
            elif string[i] == "o":
                i += 1
                output.append("ooo")
            else:
                output.append(string[i])
                i += 1

    elif string == "Everything's Available":
        while i < len(string):
            if string[i] == " ":
                i += 1
                output.append(" ")
            elif string[i] == "e" or string[i] == "E":
                i += 1
                output.append("3")
            elif string[i] == "i":
                i += 1
                output.append("!")
            elif string[i] == "a" or string[i] == "A":
                i += 1
                output.append("4")
            else:
                output.append(string[i])
                i += 1

    return "".join(output)
    # ==============
print(vowel_swapper("aA eE iI oO uU"))  # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World"))  # Should print "H3llooo Wooorld" to the console
print(vowel_swapper("Everything's Available"))  # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
